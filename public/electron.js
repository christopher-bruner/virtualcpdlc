const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const dialog = electron.dialog;

const updater = require('electron-updater');
const autoUpdater = updater.autoUpdater;

const isDev = require('electron-is-dev');
const path = require('path');

if(require('electron-squirrel-startup')) app.quit();
if(handleSquirrelEvent()) {
    return;
}

function handleSquirrelEvent() {

    if(process.argv.length === 1){
        return false;
    }

    const ChildProcess = require('child_process');
    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);

    const spawn = function(command, args) {
        let spawnedProcess, error;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {detached: true});
        } catch(error) {}

        return spawnedProcess;
    };

    const spawnUpdate = function(args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch(squirrelEvent) {

        case '--squirrel-install':
        case '--squirrel-updated':
            spawnUpdate(['--createShortcut', exeName]);

            setTimeout(app.quit(), 1000);
            return true;

        case '--squirrel-uninstall':
            spawnUpdate(['--removeShortcut', exeName]);

            setTimeout(app.quit(), 1000);
            return true;

        case '--squirrel-obsolete':
            app.quit();
            return true;
            
    }

}

function createWindow() {
    mainWindow = new BrowserWindow(mainOpts);
    mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
    mainWindow.on('closed', () => mainWindow = null);
}

let mainWindow;
const hasLock = app.requestSingleInstanceLock();

autoUpdater.requestHeaders = {"PRIVATE TOKEN": "access token"};
autoUpdater.autoDownload = true;
autoUpdater.setFeedURL({
    provider: "generic",
    url: ""
});

autoUpdater.on('update-downloaded', (event, info) => {

    const dialogOpts = {
        type: 'info',
        buttons: ['Restart', 'Later'],
        title: 'Update Available',
        message: info,
        detail: 'New version available. Restart to apply the update.'
    }

    dialog.showMessageBox(dialogOpts, (response) => {
        if(response === 0 ) autoUpdater.quitAndInstall();
    })
})

autoUpdater.on('error', (event, message) => {
    dialog.showErrorBox('Update Error', `Error updating application. ${message}`);
})

autoUpdater.checkForUpdates();

let mainOpts = {
    title: 'virtualCPDL', width: 1140, height: 912, resizable: false, frame: false, backgroundColor: '#000', autoHideMenuBar: true, disableAutoHideCursor: true,
}

if(!hasLock) {
    app.quit();
} else {

    app.on('second-instance', () => {
        if(mainWindow) {
            if(mainWindow.isMinimized()) mainWindow.restore()
            mainWindow.focus();
        }
    })

    app.on('ready', createWindow);

    app.on('activate', () => {
        if(mainWindow === null) createWindow();
    });

}

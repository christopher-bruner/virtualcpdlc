import React, { Component } from "react";
import PropTypes from 'prop-types';
import {View} from 'react-desktop/windows';
import {withRouter} from 'react-router';

import {UMMessageMap, DMMessageMap, responseMap, ResponseAttributesMap} from './MessageDefinitions';
var hash = require('object-hash');

export default function createMessage(id, to, from, time, messageType, args, response) {

    var obj = {};

    obj.id = id;
    obj.to = to;
    obj.from = from;

    obj.time = time;
    obj.messageType = messageType;
    obj.args = args;
    obj.response = response;

    return obj;

}

export function createID(message) {

    var msgHash = hash(message, {excludeKeys: function(key) {
            if(key === 'id' || key === 'args' || key === 'response' || key === 'status') {
                return true;
            }
            return false;
        }
    });
    //var rand = Math.floor(Math.random()*100)+1;

    return msgHash;

}

export function createTimeStamp() {

    let d = new Date();
    function pad(n) {
        return n < 10 ? '0' + n : n;
    }

    let hour = pad(d.getUTCHours());
    let minutes = pad(d.getUTCMinutes());
    let sec = pad(d.getUTCSeconds());

    var obj = {h: hour, m: minutes, s: sec}

    return obj;

}

/**
 * Parses message of given type and arguments to human readable string.
 */
export function parseMessage(messageType, args) {

    let el;

    el = UMMessageMap.get(messageType);
    if(el){
        return el.parseFunction(args);
    }
    
    el = DMMessageMap.get(messageType);
    if(el){
        return el.parseFunction(args);
    } else {
        return null;
    }

}

export function parseTime(time) {
    return time.h+time.m;
}

/**
 * 
 * Parses message category of given message type
 */
export function parseMessageLabel(messageType) {

    let el;

    el = UMMessageMap.get(messageType);
    if(el){
        return el.messageLabel;
    }

    el = DMMessageMap.get(messageType);
    if(el){
        return el.messageLabel;
    } else {
        return null;
    }

}

export function parseResponse(response) {

    const el = responseMap.get(response);
    if(el) {
        return el;
    }

    return null;
}

/**
 * Parses response of given type to human readable string.
 */
export function parseStatus(statusType) {

    if(statusType === -1) {
        return 'SENT';
    } else if(statusType >= 0 && statusType <= 5) {
        return 'RESPONSE RCVD';
    } else if(statusType <= -2 || statusType === 62) {
        return 'ABORTED';
    } else {
        return 'ERR';
    }

}

export function parseResponseAttribute(message) {

    let msgEl;
    let el;

    msgEl = UMMessageMap.get(message.messageType);
    if(!msgEl) {
        msgEl = DMMessageMap.get(message.messageType);
        if(!msgEl) {
            return null;
        }
    }

    el = ResponseAttributesMap.get(msgEl.responseAttribute);
    if(el) {
        return el(message);
    } else {
        return null;
    }
    
}

export const messageTypesMap = new Map([ ['uplink', 'UM'], ['downlink', 'DM'], ['info', 'FI'], ['sent', 'B1,B3,5D,5U'], ['received', 'A1,A2'], ['weather', '2U,A9'] ]);

/**
 * Filters Messages from messageList based on given messageType
 */
export function filterMessageType(messageList, messageTypeKey) {

    var arr = [];

    let messageTypes = messageTypesMap.get(messageTypeKey);
    if(messageTypes){
        messageTypes = messageTypes.trim().split(',');
    } else return;

    for (let index = 0; index < messageList.length; index++) {

        const element = messageList[index];
        if(element === undefined || element === null){
            continue;
        }

        messageTypes.forEach(function(item){
            if(element.messageType.includes(item)){arr.push(element);}
        });
        
    }

    return arr;

}

/**
 * Returns a Message from messageList based on id.
 */
export function getMessage(messageList, id) {

    for(let index = 0; index < messageList.length; index++) {
        const el = messageList[index];
        if(el.id === id) {
            return el;
        }
    }

}

class ReviewMessageItem extends Component {

    render() {

        const {
            id,
            time,
            messageType,
            args,
            response,

            history,
            location,
            match,
            staticContext,
            to,
            onClick,

            ...rest
        } = this.props;
        
        return(

            <li>
            <View style={{flex: 1, justifyContent: 'space-between', }}
                {...rest}
                onClick={(event) => {
                    onClick && onClick(event)
                    history.push(to)
                }}
            >
                <View>{`${parseTime(time)}Z`}</View>
                <View style={{flex: .5}}>{parseMessageLabel(messageType)}</View>
                <View style={{flex: .12, flexWrap: 'wrap', }}>{parseStatus(response)}</View>
            </View>
            </li>

        )

    }

}
ReviewMessageItem.propTypes = {
    time: PropTypes.string,
    messageType: PropTypes.string,
    response: PropTypes.number,
    to: PropTypes.string,
}
export const ReviewMessageListItem = withRouter(ReviewMessageItem);

class NewMessageItem extends Component {
  
    render() {

        const {
            id,
            time,
            messageType,
            args,
            response,

            history,
            location,
            match,
            staticContext,
            to,
            onClick,

            ...rest
        } = this.props;
        
        return(

            <li>
            <View style={{flex: 1, justifyContent: 'space-between', }}
                {...rest}
                onClick={(event) => {
                    onClick && onClick(event)
                    history.push(to)
                }}
            >
                <View>{`${parseTime(time)}Z`}</View>
                <View>{parseMessageLabel(messageType)}</View>
                <View></View>
            </View>
            </li>

        )

    }    
    
}
NewMessageItem.propTypes = {
    time: PropTypes.string,
    messageType: PropTypes.string,
}
export const NewMessageListItem = withRouter(NewMessageItem);
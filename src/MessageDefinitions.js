import React from 'react';
import {WUButtonBar, ANButtonBar, RButtonBar, NButtonBar} from './ButtonBar';

function createMessageDefinition(typeCode, messageLabel, parseFunction = function(){}, responseAttribute) {

    var obj = {};

    obj.typeCode = typeCode;
    obj.messageLabel = messageLabel;
    obj.parseFunction = parseFunction;
    obj.responseAttribute = responseAttribute;

    return obj;

}

export const UMMessageMap = new Map([

    /**Response Uplink Messages */
    // ['UM0', createMessageDefinition('UM0', 'UNABLE', function(){return 'UNABLE'})],
    // ['UM1', createMessageDefinition('UM1', 'STANDBY', function(){return 'STANDBY'})],
    // ['UM2', createMessageDefinition('UM2', 'REQUEST DEFERRED', function(){return 'REQUEST DEFERRED'})],
    // ['UM3', createMessageDefinition('UM3', 'ROGER', function(){return 'ROGER'})],
    // ['UM4', createMessageDefinition('UM4', 'AFFIRM', function(){return 'AFFIRM'})],
    // ['UM5', createMessageDefinition('UM5', 'NEGATIVE', function(){return 'NEGATIVE'})],
    // ['UM159', createMessageDefinition('UM159', 'ERROR', function(args){return 'ERROR '+args})],

    ['UM143', createMessageDefinition('UM143', 'CONFIRM REQUEST', function(){return 'CONFIRM REQUEST'})],
    /**System Response Uplink Messages */
    ['UM169', createMessageDefinition('UM169', 'FREE TEXT', function(args){return args}, 'WU')],
    ['UM74', createMessageDefinition('UM74', 'PROCEED DIRECT TO', function(args){return 'PROCEED DIRECT '+args}, 'WU')],
    ['UM79', createMessageDefinition('UM79', 'CLEARED TO', function(args = []){
            let c,r,a,f,t, callsign;
            callsign = args[args.length-1];

            c = args[0];
            r = args[1];
            a = args[2];
            f = args[3];
            t = args[4];

            return `${callsign} CLEARED TO ${c} ${r} ${a} ${f} ${t}`;
    },
        'WU')],
    ['UM99', createMessageDefinition('UM99', 'EXPECT', function(args){return 'EXPECT '+args}, 'R')],
    ['UM137', createMessageDefinition('UM137', 'CONFIRM ASSIGNED ROUTE', function(){return 'CONFIRM ASSIGNED ROUTE'}, 'Y')],
    ['UM147', createMessageDefinition('UM147', 'REQUEST POSITION REPORT', function(){return 'REQUEST POSITION REPORT'}, 'Y')],
    ['UM64', createMessageDefinition('UM64', 'OFFSET', function(){})],
    ['UM72', createMessageDefinition('UM72', 'RESUME OWN NAVIGATION', function(){return 'RESUME OWN NAVIGATION'}, 'WU')],
    ['UM82', createMessageDefinition('UM82', 'CLEARED TO DEVIATE', function(){}, 'WU')],
    ['UM94', createMessageDefinition('UM94', 'TURN HEADING', function(args){return 'FLY HEADING '+args}, 'WU')],
    ['UM96', createMessageDefinition('UM96', 'FLY PRESENT HEADING', function(){return 'FLY PRESENT HEADING'}, 'WU')],
    ['UM19', createMessageDefinition('UM19', 'MAINTAIN ALTITUDE', function(args){return 'MAINTAIN ALTITUDE '+args}, 'WU')],
    ['UM30', createMessageDefinition('UM30', 'MAINTAIN BLOCK', function(args = []){return 'MAINTAIN BLOCK '+args[0]+' TO '+args[1]}, 'WU')],
    ['UM20', createMessageDefinition('UM20', 'CLIMB TO AND MAINTAIN', function(args){return 'CLIMB AND MAINTAIN '+args}, 'WU')],
    ['UM23', createMessageDefinition('UM23', 'DESCEND TO', function(args){return 'DESCEND AND MAINTAIN '+args}, 'WU')],
    ['UM46', createMessageDefinition('UM46', 'CROSS AT', function(args = []){return 'CROSS '+args[0]+' AT '+args[1]}, 'WU')],
    ['UM47', createMessageDefinition('UM47', 'CROSS AT OR ABOVE', function(args = []){return 'CROSS '+args[0]+' AT OR ABOVE '+args[1]}, 'WU')],
    ['UM48', createMessageDefinition('UM48', 'CROSS AT OR BELOW', function(args = []){return 'CROSS '+args[0]+'AT OR BELOW '+args[1]}, 'WU')],
    ['UM133', createMessageDefinition('UM133', 'REPORT PRESENT LEVEL', function(){return 'REPORT PRESENT LEVEL'}, 'Y')],
    ['UM135', createMessageDefinition('UM135', 'CONFIRM ASSIGNED ALTITUDE', function(){return 'CONFIRM ASSIGNED ALTITUDE'}, 'Y')],
    ['UM106', createMessageDefinition('UM106', 'MAINTAIN SPEED', function(args){return 'MAINTAIN SPEED '+args}, 'WU')],
    ['UM107', createMessageDefinition('UM107', 'MAINTAIN PRESENT SPEED', function(){return 'MAINTAIN PRESENT SPEED'}, 'WU')],
    ['UM108', createMessageDefinition('UM108', 'MAINTAIN SPEED', function(args){return 'MAINTAIN SPEED '+args+' OR GREATER'}, 'WU')],
    ['UM109', createMessageDefinition('UM109', 'MAINTAIN SPEED', function(args){return 'MAINTAIN SPEED '+args+' OR LESS'}, 'WU')],
    ['UM116', createMessageDefinition('UM116', 'RESUME NORMAL SPEED', function(){return 'RESUME NORMAL SPEED'}, 'WU')],
    ['UM134', createMessageDefinition('UM134', 'CONFIRM SPEED', function(){return 'CONFIRM SPEED'}, 'Y')],
    ['UM153', createMessageDefinition('UM153', 'ALTIMETER', function(args){return 'ALTIMETER '+args}, 'N')],
    ['UM154', createMessageDefinition('UM154', 'RADAR SERVICE TERMINATED', function(){return 'RADAR SERVICE TERMINATED'}, 'N')],
    ['UM155', createMessageDefinition('UM155', 'RADAR CONTACT', function(args){return 'RADAR CONTACT '+args}, 'N')],
    ['UM156', createMessageDefinition('UM156', 'RADAR CONTACT LOST', function(){return 'RADAR CONTACT LOST'}, 'N')],
    ['UM123', createMessageDefinition('UM123', 'SQUAWK', function(args){return 'SQUAWK '+args}, 'WU')],
    ['UM125', createMessageDefinition('UM125', 'SQUAWK ALTITUDE', function(){return 'SQUAWK ALTITUDE'}, 'WU')],
    ['UM126', createMessageDefinition('UM126', 'STOP ALTITUDE SQUAWK', function(){return 'STOP ALTITUDE SQUAWK'}, 'WU')],
    ['UM144', createMessageDefinition('UM144', 'CONFIRM SQUAWK', function(){return 'CONFIRM SQUAWK'}, 'Y')],
    ['UM179', createMessageDefinition('UM179', 'SQUAWK IDENT', function(){return 'SQUAWK IDENT'}, 'WU')],
    ['UM117', createMessageDefinition('UM117', 'CONTACT', function(args = []){return 'CONTACT '+args[0]+' '+args[1]}, 'WU')],
    ['UM120', createMessageDefinition('UM120', 'MONITOR', function(args = []){return 'MONITOR '+args[0]+' '+args[1]}, 'WU')],
    ['UM157', createMessageDefinition('UM157', 'CHECK STUCK MIC', function(){return 'CHECK STUCK MIC'}, 'N')],

    ['2U', createMessageDefinition('2U', 'WEATHER REPORT', function(args){return args}, 'N')],
    ['A9', createMessageDefinition('A9', 'ATIS', function(args){return args}, 'N')],
    ['A1', createMessageDefinition('A1', 'OCEANIC CLEARANCE', function(args = []){return 'OCEANIC CLEARANCE'}, 'N')],
    ['A2', createMessageDefinition('A2', 'DEPARTURE CLEARANCE', function(args = []){return 'DEPARTURE CLEARANCE'}, 'N')],
    ['FN_AK', createMessageDefinition('FN_AK', 'LOGON RESPONSE', function(){return 'LOGON RESPONSE'})],

]);

/**
 * Response attributes applicable to Uplink/Downlink Messages.
 */
export const ResponseAttributesMap = new Map([

    ['WU', function(message){ return(<WUButtonBar message={message} />)}],
    ['AN', function(message){ return(<ANButtonBar message={message} />)}],
    ['R', function(message){ return (<RButtonBar message={message}/>)}],
    ['Y', function(message){ return (<NButtonBar message={message}/>)}],
    ['N', function(message){ return (<NButtonBar message={message}/>)}],

]);

export const responseMap = new Map([
    [62, 'ERR'],
    [-1, 'SNT'],
    [0, 'WIL'],
    [1, 'UNA'],
    [2, 'SBY'],
    [3, 'ROG'],
    [4, 'AFF'],
    [5, 'NEG'],
    [-99, 'FAI'],
    [-2, 'TIM'],
    [-3, 'HLD'],
]);

export const DMMessageMap = new Map([

    /**Response Downlink Messages */
    // ['DM0', createMessageDefinition('DM0', 'WILCO', function(){return 'WILCO'})],
    // ['DM1', createMessageDefinition('DM1', 'UNABLE', function(){return 'UNABLE'})],
    // ['DM2', createMessageDefinition('DM2', 'STANDBY', function(){return 'STANDBY'})],
    // ['DM3', createMessageDefinition('DM3', 'ROGER', function(){return 'ROGER'})],
    // ['DM4', createMessageDefinition('DM4', 'AFFIRM', function(){return 'AFFIRM'})],
    // ['DM5', createMessageDefinition('DM5', 'NEGATIVE', function(){return 'NEGATIVE'})],

    /**System Response Downlink Messages */
    ['DM62', createMessageDefinition('DM62', 'ERROR', function(args){return 'ERROR '+args})],
    ['DM63', createMessageDefinition('DM63', 'ERROR', function(){return 'NOT CURRENT DATA AUTHORITY'})],
    ['FN_CON', createMessageDefinition('FN_CON', 'LOGON REQUEST', function(){return 'LOGON REQUEST'})],

    //Altitude Request
    ['DM6', createMessageDefinition('DM6', 'ALTITUDE REQUEST', function(args = []){return 'REQUEST ALTITUDE '+args[0]+' '+args.slice(1).join(' ')}, 'Y')],
    ['DM7', createMessageDefinition('DM7', 'ALTITUDE REQUEST', function(args = []){return 'REQUEST BLOCK '+args[0]+' TO '+args[1]+' '+args.slice(1).join(' ')}, 'Y')],
    ['DM9', createMessageDefinition('DM9', 'ALTITUDE REQUEST', function(args = []){return 'REQUEST CLIMB '+args[0]+' '+args.slice(1).join(' ')}, 'Y')],
    ['DM10', createMessageDefinition('DM10', 'ALTITUDE REQUEST', function(args = []){return 'REQUEST DESCENT '+args.join(' ')}, 'Y')],

    //Route Request
    ['DM15', createMessageDefinition('DM15', 'ROUTE REQUEST', function(args = []){return 'REQUEST OFFSET '+args[0]+' '+args.slice(1).join(' ')}, 'Y')],
    ['DM22', createMessageDefinition('DM22', 'ROUTE REQUEST', function(args){return 'REQUEST DIRECT '+args}, 'Y')],
    ['DM24', createMessageDefinition('DM24', 'ROUTE REQUEST', function(args = []){return 'REQUEST '+args[0]+' TO '+args[1]}, 'Y')],
    ['DM27', createMessageDefinition('DM27', 'ROUTE REQUEST', function(args){return 'REQUEST WEATHER DEVIATION UP TO '+args+'MI'}, 'Y')],
    ['DM70', createMessageDefinition('DM70', 'ROUTE REQUEST', function(args){return 'REQUEST HEADING'+args}, 'Y')],
    ['DM71', createMessageDefinition('DM71', 'ROUTE REQUEST', function(args){return 'REQUEST TRACK '+args}, 'Y')],

    //Speed Request
    ['DM18', createMessageDefinition('DM18', 'SPEED REQUEST', function(args){return 'REQUEST SPEED '+args}, 'Y')],

    //Clearance Request
    ['DM25', createMessageDefinition('DM25', 'REQUEST CLEARANCE', function(){return 'REQUEST CLEARANCE'}, 'Y')],

    //Expect Request
    ['DM49', createMessageDefinition('DM49', 'WHEN CAN WE EXPECT', function(args = []){return 'WHEN CAN WE EXPECT '+args[0]}, 'Y')], //spd or alt
    ['DM51', createMessageDefinition('DM51', 'WHEN CAN WE EXPECT', function(args = []){return 'WHEN CAN WE EXPECT BACK ON ROUTE'}, 'Y')],
    ['DM52', createMessageDefinition('DM52', 'WHEN CAN WE EXPECT', function(args = []){return 'WHEN CAN WE EXPECT LOWER ALTITUDE'}, 'Y')],
    ['DM53', createMessageDefinition('DM53', 'WHEN CAN WE EXPECT', function(args = []){return 'WHEN CAN WE EXPECT HIGHER ALTITUDE'}, 'Y')],
    ['DM54', createMessageDefinition('DM54', 'WHEN CAN WE EXPECT', function(args = []){return 'WHEN CAN WE EXPECT CRUISE CLIMB TO '+args[0]}, 'Y')],

    //Voice Request
    ['DM20', createMessageDefinition('DM20', 'REQUEST VOICE CONTACT', function(){return 'REQUEST VOICE CONTACT'}, 'Y')],

    //ATC Report Requests
    ['DM28', createMessageDefinition('DM28', 'LEAVING', function(args){return 'LEAVING '+args}, 'N')],
    ['DM29', createMessageDefinition('DM29', 'CLIMBING TO', function(args){return 'CLIMBING TO '+args}, 'N')],
    ['DM30', createMessageDefinition('DM30', 'DESCENDING TO', function(args){return 'DESCENDING TO '+args}, 'N')],
    ['DM34', createMessageDefinition('DM34', 'PRESENT SPEED', function(args){return 'PRESENT SPEED '+args}, 'N')],
    ['DM37', createMessageDefinition('DM37', 'LEVEL', function(args){return 'LEVEL AT '+args}, 'N')],
    ['DM38', createMessageDefinition('DM38', 'ASSIGNED LEVEL', function(args){return 'ASSIGNED LEVEL '+args}, 'N')],
    ['DM39', createMessageDefinition('DM39', 'ASSIGNED SPEED', function(args){return 'ASSIGNED SPEED '+args}, 'N')],
    ['DM40', createMessageDefinition('DM40', 'ASSIGNED ROUTE', function(args){return 'ASSIGNED ROUTE '+args}, 'N')],
    ['DM41', createMessageDefinition('DM41', 'BACK ON ROUTE', function(){return 'BACK ON ROUTE'}, 'N')],
    ['DM47', createMessageDefinition('DM47', 'SQUAWKING', function(args){return 'SQUAWKING '+args}, 'N')],
    ['DM69', createMessageDefinition('DM69', 'CLEAR OF WEATHER', function(){return 'CLEAR OF WEATHER'}, 'N')],

    //Position Report
    ['DM48', createMessageDefinition('DM48', 'POSITION REPORT', function(args = []){return args.join(' ')}, 'N')],

    //Free Text
    ['DM67', createMessageDefinition('DM67', 'FREE TEXT', function(args){return args}, 'N')],

    // ['DM65', createMessageDefinition('DM65', 'DUE TO WEATHER', function(){return 'DUE TO WEATHER'}, 'N')],
    // ['DM66', createMessageDefinition('DM66', 'DUE TO PERFORMANCE', function(){return 'DUE TO AIRCRAFT PERFORMANCE'}, 'N')],
    ['DM23', createMessageDefinition('DM23', 'REQUEST', function(args){return 'REQUEST '+args}, 'Y')],

    ['5U', createMessageDefinition('5U', 'WEATHER REQUEST', function(args){return args+' WEATHER REQUEST'}, 'N')],
    ['5D', createMessageDefinition('5D', 'ATIS REQUEST', function(args){return args+' ATIS REQUEST'}, 'N')],
    ['B1', createMessageDefinition('B1', 'OCEANIC CLEARANCE REQUEST', function(args = []){return 'REQUEST OCEANIC CLEARANCE '+args.join(' ')}, 'N')],
    ['B3', createMessageDefinition('B3', 'DEPARTURE CLEARANCE REQUEST', function(args = []){return 'REQUEST DEPARTURE CLEARANCE '+args.join(' ')}, 'N')],

]);

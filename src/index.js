import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {MemoryRouter} from 'react-router-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import allReducers, { receiveMessage, receiveUpdate, logonAccept, receiveTransferRequest, receiveTransferReject, controllerTransfer, controllerLogoff } from './reducer';

import {Howl} from 'howler';
import io from 'socket.io-client';
import socketIO from 'socket.io-redux';

//const SOCKET_URL = 'http://74.91.119.61:3697';
const SOCKET_URL = 'http://localhost:3697';

const socketConnection = io.connect(SOCKET_URL);

export const store = createStore(allReducers,
    applyMiddleware(socketIO(socketConnection)
));

const acarsSound = new Howl({
    src: ['acars.wav']
});

socketConnection.on('connect', function(){

    // socketConnection.on('disconnect', () => {

    // });
    
    socketConnection.on('receive_message', (message) => {
        acarsSound.play();
        store.dispatch(receiveMessage(message));

        if(message.messageType === 'UM154') {
            store.dispatch(controllerLogoff());
        }
    });

    socketConnection.on('receive_update', (message) => {
        store.dispatch(receiveUpdate(message));
    });

    socketConnection.on('logon_accept', (message) => {
        store.dispatch(logonAccept(message));
        acarsSound.play();
    });

    // socketConnection.on('controller_logoff', (message) => {
    //     store.dispatch(controllerLogoff());
    // });
    socketConnection.on('transfer_request', (message) => {
        console.log(message);
        if(message.includes(store.getState().cpdlc.activeConnection)){
            return;
        }

        store.dispatch(receiveTransferRequest(message));
    });
    socketConnection.on('transfer_reject', () => {
        store.dispatch(receiveTransferReject);
    });
    socketConnection.on('controller_transfer', () => {
        if(!store.getState().cpdlc.inactiveConnection){
            return;
        }
        console.log(store.getState().cpdlc);
        store.dispatch(controllerTransfer);
    });

});

ReactDOM.render(

    <Provider store={store}>
        <MemoryRouter>
            <App />
        </MemoryRouter>
    </Provider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

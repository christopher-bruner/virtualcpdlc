import React from 'react';
import {View} from 'react-desktop/windows';
import '../App.css';

import SubMenuButton from  '../SubMenuButton';

const views = [
    {id: 'departureclearancerequest', label: 'DEPARTURE CLEARANCE REQ'},
    {id: 'oceanicclearancerequest', label: 'OCEANIC CLEARANCE REQ'},
    {id: 'atisrequest', label: 'ATIS REQUEST'},
    {id: '', label: 'TWIP REQUEST'},
    {id: '', label: 'PUSHBACK REQUEST'},
    {id: '', label: 'EXPECTED TAXI REQUEST'},
]

export const FlightMenu = () => {

    return (

        <View style={{flexDirection: 'column', flex: 1,}}>

            <View style={{color: 'white', alignSelf: 'center', fontSize: 30, paddingBottom: 10}}>FLIGHT INFORMATION</View>

            <View style={{flexDirection: 'row', color: 'white', width: '100%', height: '100%', fontSize: 25,}}>
            
                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                    {views.slice(0, 3).map(({id, label}) => {
                        return(<SubMenuButton to={'/flight/'+id}>{label}</SubMenuButton>)
                    })}
                </View>
            
                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                    {views.slice(3, 6).map(({id, label}) => {
                        if(id)
                            return <SubMenuButton to={'/flight/'+id}>{label}</SubMenuButton>
                        else
                            return <SubMenuButton>{label}</SubMenuButton>
                    })}
                </View>

            </View>

        </View>      

    )

}

export default FlightMenu;
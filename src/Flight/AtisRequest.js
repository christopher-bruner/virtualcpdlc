import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import AtisRequestForm from '../Forms/AtisRequestForm';

class AtisRequest extends Component {
    
    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label="ATIS REQUEST" />
                <AtisRequestForm />
            </View>            

        )
    }

}

export default AtisRequest;
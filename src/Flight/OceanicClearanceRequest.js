import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import OceanicClearanceRequestForm from '../Forms/OceanicClearanceRequestForm';

class OceanicClearanceRequest extends Component {
    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label="OCEANIC CLEARANCE REQUEST" />
                <OceanicClearanceRequestForm />
            </View>

        )
    }
}

export default OceanicClearanceRequest;
import React, { Component } from 'react';
import {View} from 'react-desktop/windows';

export default class UTCClock extends Component {

    constructor() {
        super();
        this.state = {currentTime: null}
    }

    componentWillMount() {
        this.getCurrentTime();
    }

    getCurrentTime = () => {

        function pad(n) {
            return n < 10 ? '0' + n : n
        }

        let hour = new Date().getUTCHours();
        let minutes = new Date().getUTCMinutes();

        this.setState({ currentTime: pad(hour)+''+ pad(minutes)+'Z' });

    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.getCurrentTime();
        }, 1000);
    }

    render() {
        return (

            <View style={{
                flex: .10, fontSize: 30, color: 'white', alignItems: 'flex-start',
            }}>{this.state.currentTime}</View>

        );
    }

}
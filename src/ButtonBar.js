import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './App.css';
import {View} from 'react-desktop/windows';

import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {submit, reset} from 'redux-form';
import { store } from './index';
import { updateMessage, removeMessage, viewMessage } from './reducer';

class ButtonBar extends Component {

    render() {

        const {dispatch} = this.props;

        return(
            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <button onClick={() => dispatch(submit(this.props.formName))}>SUBMIT</button>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => dispatch(reset(this.props.formName))} style={{alignContent: 'flex-end'}}>RESET</button>
                    <button onClick={() => this.props.history.go(-1)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>RETURN</button>
                    <button onClick={() => this.props.history.push('/')} style={{alignContent: 'flex-end'}}>EXIT MENU</button>
                </View>
            </View>
        )

    }

}
ButtonBar.propTypes = {
    formName: PropTypes.string.isRequired,
}
export default connect()(withRouter(ButtonBar));

class StatusButtons extends Component {

    render() {

        const{statusLabel} = this.props;

        return(
            <View className='Status-label' style={{flex: 1}}>{statusLabel}</View>
        )
    }
}
StatusButtons.propTypes = {
    statusLabel: PropTypes.string.isRequired,
}
export const StatusButtonBar = connect()(withRouter(StatusButtons));

class ExitButton extends Component {

    render() {

//        const {dispatch} = this.props;

        return(
            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => this.props.history.go(-1)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>EXIT MENU</button>
                </View>
            </View>
        )

    }

}
export const ExitButtonBar = withRouter(ExitButton);

class WUButtons extends Component {

    constructor(props) {
        super(props);
        this.sendWilco = this.sendWilco.bind(this);
        this.sendUnable = this.sendUnable.bind(this);
        this.sendStandby = this.sendStandby.bind(this);
    }

    sendWilco(message) {
        message.response = 0;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));

        this.props.history.go(-1);
    }
    
    sendUnable(message) {
        message.response = 1;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }
    
    sendStandby(message) {
        message.response = 2;

        store.dispatch(updateMessage(message));
        //store.dispatch(viewMessage(message.id));
        //store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    render() {

        const {message} = this.props;

        return(

            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between', color: 'white'}}>
                <button onClick={() => this.sendWilco(message)}>WILCO</button>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => this.sendUnable(message)}>UNABLE</button>
                    <button onClick={() => this.sendStandby(message)}>STANDBY</button>
                    <button onClick={() => this.props.history.go(-1)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>CANCEL</button>
                </View>
            </View>

        )

    }

}
export const WUButtonBar = connect()(withRouter(WUButtons));

class ANButtons extends Component {

    constructor(props) {
        super(props);
        this.sendAffirm = this.sendAffirm.bind(this);
        this.sendReject = this.sendReject.bind(this);
        this.sendStandby = this.sendStandby.bind(this);
    }

    sendAffirm(message) {
        message.response = 4;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }
    
    sendReject(message) {
        message.response = 5;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    sendStandby(message) {
        message.response = 2;

        store.dispatch(updateMessage(message));
        //store.dispatch(viewMessage(message.id));
        //store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    render() {

        const {message} = this.props;

        return(

            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <button onClick={() => this.sendAffirm(message)}>ACCEPT</button>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => this.sendReject(message)}>REJECT</button>
                    <button onClick={() => this.sendStandby(message)}>STANDBY</button>
                    <button onClick={() => this.props.history.go(-2)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>CANCEL</button>
                </View>
            </View>

        )

    }

}
export const ANButtonBar = connect()(withRouter(ANButtons));

class RButtons extends Component {

    constructor(props) {
        super(props);
        this.sendRoger = this.sendRoger.bind(this);
        this.sendUnable = this.sendUnable.bind(this);
        this.sendStandby = this.sendStandby.bind(this);
    }

    sendRoger(message) {
        message.response = 3;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    sendUnable(message) {
        message.response = 1;

        store.dispatch(updateMessage(message));
        store.dispatch(viewMessage(message.id));
        store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    sendStandby(message) {
        message.response = 2;
        
        store.dispatch(updateMessage(message));
        //store.dispatch(viewMessage(message.id));
        //store.dispatch(removeMessage(message.id));
        this.props.history.go(-1);
    }

    render() {

        const {message} = this.props;

        return(

            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <button onClick={() => this.sendRoger(message)}>ROGER</button>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => this.sendUnable(message)}>UNABLE</button>
                    <button onClick={() => this.sendStandby(message)}>STANDBY</button>
                    <button onClick={() => this.props.history.go(-1)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>CANCEL</button>
                </View>
            </View>

        )

    }

}
export const RButtonBar = connect()(withRouter(RButtons));

class NButtons extends Component {

    render() {

        return(

            <View className='Footer' style={{flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <View style={{alignContent: 'flex-end'}}>
                    <button onClick={() => this.props.history.go(-1)} style={{alignContent: 'flex-end', marginLeft: 10, marginRight: 10}}>CANCEL</button>
                </View>
            </View>

        )

    }

}
export const NButtonBar = connect()(withRouter(NButtons));
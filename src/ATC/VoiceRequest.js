import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import VoiceRequestForm from '../Forms/VoiceRequestForm';

class VoiceRequest extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>

                <Header label="VOICE CONTACT REQUEST" />
                <VoiceRequestForm />

            </View>

        )
    }

}

export default VoiceRequest;
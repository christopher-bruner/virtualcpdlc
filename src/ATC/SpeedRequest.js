import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import SpeedRequestForm from '../Forms/SpeedRequestForm';

class SpeedRequest extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%'}}>

                <Header label="SPEED REQUEST" />
                <SpeedRequestForm />

            </View>

        )
    }

}

export default SpeedRequest;
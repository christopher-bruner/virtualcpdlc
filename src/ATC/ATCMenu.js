import React from 'react';
import {View} from 'react-desktop/windows';
import '../App.css';

import SubMenuButton from '../SubMenuButton';

const views = [
    {id: 'altituderequest', label: 'ALTITUDE REQUEST'},
    {id: 'routerequest', label: 'ROUTE REQUEST'},
    {id: 'speedrequest', label: 'SPEED REQUEST'},
    {id: 'clearancerequest', label: 'CLEARANCE REQUEST'},

    {id: 'expectrequest', label: 'WHEN CAN WE EXPECT'},
    {id: 'voicerequest', label: 'VOICE CONTACT REQUEST'},
    {id: 'logonstatus', label: 'LOGON / STATUS'},

    {id: '', label: 'EMERGENCY REPORT'},
    {id: 'requestedreports', label: 'ATC REQUESTED REPORTS...'},
    {id: 'positionreport', label: 'POSITION REPORT'},
    {id: 'freetext', label: 'FREE TEXT MESSAGE'},
]

const ATCMenu = () => {

        return (

            <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>

                <View style={{color: 'white', alignSelf: 'center', fontSize: 30, paddingBottom: 10}}>ATC</View>

                <View style={{flexDirection: 'row', color: 'white', width: '100%', fontSize: 25, }}>

                    <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px',}}>
                        {views.slice(0, 4).map(({id, label}) =>
                            <SubMenuButton style={{height: 300,}} to={'/atc/'+id}>{label}</SubMenuButton>
                        )}
                    </View>

                    <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                        {views.slice(4, 7).map(({id, label}) =>
                            <SubMenuButton style={{height: 300,}} to={'/atc/'+id}>{label}</SubMenuButton>
                        )}
                    </View>

                    <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                        {views.slice(7, 11).map(({id, label}) => {
                            if(id)
                                return <SubMenuButton style={{height: 300,}} to={'/atc/'+id}>{label}</SubMenuButton>
                            else
                                return <SubMenuButton style={{height: 300,}}>{label}</SubMenuButton>
                        })}  
                    </View>

                </View>

            </View>

        )

}

export default ATCMenu;
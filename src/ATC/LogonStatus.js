import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import LogonForm from '../Forms/LogonForm';
import {connect} from 'react-redux';

class LogonStatus extends Component {

    
    render() {

        const cda = this.props.activeCenter.split('_');
        const nda = this.props.nextCenter.split('_');

        return(

            <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>

                <Header label="ATC LOGON / STATUS" />

                <View style={{ flex: 1, paddingLeft: 20, marginTop: 25, flexDirection: 'column', color: 'white', fontSize: 25, textTransform: 'uppercase',}}>
                    ACTIVE CENTER: {cda[0] || ''}
                    <View style={{paddingLeft: 18, marginTop: 25 }}>NEXT CENTER: {nda[0] || ''}</View>
                </View>
                <View style={{ flex: 1, paddingLeft: 20, color: 'white', fontSize: 25, marginTop: 35, marginLeft: 90,}}>
                    ATC CONNECTION: {this.props.activeCenter ? 'ESTABLISHED' : this.props.logonStatus === 0 ?
                     'ESTABLISHING' : 'NOT ESTABLISHED'}
                </View>

                <LogonForm />

            </View>

        )
    }

}

const mapStateToProps = state => ({
    activeCenter: state.cpdlc.activeConnection,
    nextCenter: state.cpdlc.inactiveConnection,
    logonStatus: state.cpdlc.logonStatus,
})

export default connect(mapStateToProps)(LogonStatus);
import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import AltitudeRequestForm from '../Forms/AltitudeRequestForm';

class AltitudeRequest extends Component {

    render() {
        return(

            <View style={{flexDirection: 'column', flex: 1, height: '100%'}}>

                <Header label="ALTITUDE REQUEST" />            
                <AltitudeRequestForm />
                
            </View>

        )
    }

}

export default AltitudeRequest;
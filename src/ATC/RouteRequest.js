import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import RouteRequestForm from '../Forms/RouteRequestForm';

class RouteRequest extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%'}}>

                <Header label="ROUTE REQUEST" />
                <RouteRequestForm />

            </View>

        )
    }

}

export default RouteRequest;
import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import '../App.css';
import Header from '../Header';
import ExpectRequestForm from '../Forms/ExpectRequestForm';

class ExpectRequest extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>

                <Header label="WHEN CAN WE EXPECT" />
                <ExpectRequestForm />

            </View>
        )
    }

}

export default ExpectRequest;
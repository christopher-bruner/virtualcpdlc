import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import ClearanceRequestForm from '../Forms/ClearanceRequestForm';
import Header from '../Header';

class ClearanceRequest extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>

                <Header label="CLEARANCE REQUEST" />
                <ClearanceRequestForm />

            </View>
        )
    }

}

export default ClearanceRequest;
import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import FreeTextForm from '../Forms/FreeTextForm';
import ButtonBar from '../ButtonBar';

class FreeTextMessage extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>

                <Header label="FREE TEXT MESSAGE" />
                
                <View style={{flex: 1, height: '100%', width: '100%', flexDirection: 'row'}}>
                    <FreeTextForm />
                </View>

                <ButtonBar formName='freetextForm' />

            </View>

        )
    }

}

export default FreeTextMessage;
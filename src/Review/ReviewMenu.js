import React from 'react';
import {View} from 'react-desktop/windows';
import '../App.css';

import {connect} from 'react-redux';

import {filterMessageType} from '../Message';
import SubMenuButton from '../SubMenuButton';

const views = [
    {id: 'uplink', label: 'ATC UPLINK...'},
    {id: 'downlink', label: 'ATC DOWNLINK...'},
    {id: 'info', label: 'FLIGHT INFORMATION'},
    {id: 'sent', label: 'SENT...'},
    {id: 'received', label: 'RECEIVED...'},
    {id: 'weather', label: 'WEATHER...'},
]

export const ReviewMenu = (props) => {

    const {
        messageList
    } = props;

    return(

        <View style={{flexDirection: 'column', flex: 1,}}>

            <View style={{color: 'white', alignSelf: 'center', fontSize: 30, paddingBottom: 10}}>REVIEW</View>

            <View style={{flexDirection: 'row', color: 'white', width: '100%', height: '100%', fontSize: 25}}>
            
                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px',}}>
                    {views.slice(0, 2).map(({id, label}) => {

                        if(filterMessageType(messageList, id).length > 0){
                            return <SubMenuButton style={{height: 300,}} to={'/review/'+id}>{label}</SubMenuButton>
                        } else {
                            return <SubMenuButton style={{height: 300,}}>{label}</SubMenuButton>

                        }

                    }
                    )}
                </View>

                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px',}}>
                    {views.slice(2, 3).map(({id, label}) => {

                        if(filterMessageType(messageList, id).length > 0){
                            return <SubMenuButton style={{height: 300,}} to={'/review/'+id}>{label}</SubMenuButton>
                        } else {
                            return <SubMenuButton style={{height: 300,}}>{label}</SubMenuButton>

                        }
                                                
                    }
                    )}
                </View>

                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px',}}>
                    {views.slice(3, 6).map(({id, label}) => {

                        if(filterMessageType(messageList, id).length > 0){
                            return <SubMenuButton style={{height: 300,}} to={'/review/'+id}>{label}</SubMenuButton>
                        } else {
                            return <SubMenuButton style={{height: 300,}}>{label}</SubMenuButton>

                        }
                                                
                    }
                    )}
                </View>                

            </View>

        </View>

    )

}

const mapStateToProps = state => ({
    messageList: state.cpdlc.messageHistory,
});

export default connect(mapStateToProps)(ReviewMenu);
import React, {Component} from 'react';
import '../App.css';
import {getMessage, parseStatus, parseMessage, parseMessageLabel} from '../Message';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import {ExitButtonBar} from '../ButtonBar';

import {connect} from 'react-redux';

class ReviewView extends Component {

    render() {
        
        const {
            match,
            messageHistory,
        } = this.props;

        let header;
        var msg = getMessage(messageHistory, match.params.id)
        
        if(msg.messageType.includes("DM")){header = <Header label='VERIFY REQUEST' text={parseStatus(msg.response)} />} 
        else if(msg.messageType.includes("UM")){header = <Header label={parseMessageLabel(msg.messageType)} />}

        return(
            <View style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                {header}
                <View style={{marginLeft: '25%', marginTop: '15px', color: 'white', fontSize: 25,}}>
                    {parseMessage(msg.messageType, msg.args)}
                </View>
                <ExitButtonBar />
            </View>
        )

    }
}

const mapStateToProps = state => ({
    messageHistory: state.cpdlc.messageHistory,
});

export default connect(mapStateToProps)(ReviewView);

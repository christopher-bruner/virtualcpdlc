import React from 'react';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import {ExitButtonBar} from '../ButtonBar';
import {ReviewMessageListItem, filterMessageType} from '../Message';
import '../App.css';

import {connect} from 'react-redux';

const ReviewList = (props) => {

        const {
            match,
            messageList,
        } = props;
    
        return(
    
            <View style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label='REVIEW' />
                <ul>
                {
                    filterMessageType(messageList, match.params.id).map(({id, time, messageType, args, response}) =>
                    {
                        return (<ReviewMessageListItem to={'/review/view/'+id} time={time} messageType={messageType} args={args} response={response} />)
                    })
                }
                </ul>
                <ExitButtonBar />
            </View>
        )

}

const mapStateToProps = state => ({
    messageList: state.cpdlc.messageHistory,
});

export default connect(mapStateToProps)(ReviewList);
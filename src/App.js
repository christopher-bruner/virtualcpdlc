import React, { Component } from 'react';
import './App.css';
import {View} from 'react-desktop/windows';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';

import {Route} from 'react-router-dom';
import MenuButton from './MenuButton';

import ATCMenu from './ATC/ATCMenu';
import AltitudeRequest from './ATC/AltitudeRequest';
import RouteRequest from './ATC/RouteRequest';
import SpeedRequest from './ATC/SpeedRequest';
import ClearanceRequest from './ATC/ClearanceRequest';
import ExpectRequest from './ATC/ExpectRequest';
import VoiceRequest from './ATC/VoiceRequest';
import LogonStatus from './ATC/LogonStatus';
import PositionReport from './ATC/PositionReport';
import FreeTextMessage from './ATC/FreeTextMessage';

import FlightMenu from './Flight/FlightMenu';
import DepartureClearanceRequest from './Flight/DepartureClearanceRequest';
import OceanicClearanceRequest from './Flight/OceanicClearanceRequest';
import AtisRequest from './Flight/AtisRequest';

import ReviewMenu from './Review/ReviewMenu';
import ReviewList from './Review/ReviewList';
import ReviewView from './Review/ReviewView';

import ManagerMenu from './Manager/ManagerMenu';
import Master from './Manager/Master';
import AutoMessages from './Manager/AutoMessages';
import SystemInformation from './Manager/SystemInformation';

import MessagesMenu from './Messages/MessagesMenu';
import MessageView from './Messages/MessageView';

import {TitleBar} from 'react-desktop/windows';

const remote = window.require('electron').remote;
let w = remote.getCurrentWindow();

class App extends Component {

  close = () => w.close();
  minimize = () => w.minimize();
  render() {

      return(

        <View style={{flexDirection: 'column', width: '100%', height: '100%', color: 'white', fontSize: 25}}>

          <TitleBar 
            title="virtualCPDLC"
            controls
            isMaximized={true}
            theme="dark"
            onCloseClick={this.close}
            onMinimizeClick={this.minimize}
          />

          <View style={{flex: 1, flexGrow: 1, marginTop: 10}}>
            <MenuButton to='/atc'>ATC</MenuButton>
            <MenuButton to='/flight'>FLIGHT INFORMATION</MenuButton>
            <MenuButton>COMPANY</MenuButton>
          </View>
          <View style={{flex: 1, flexGrow: 1}}>
            {this.props.messageHistory.length === 0 ?
            <MenuButton>REVIEW</MenuButton> :
            <MenuButton to='/review'>REVIEW</MenuButton>
            }
            <MenuButton to='/manager'>MANAGER</MenuButton>
            {this.props.newMessages.length === 0 ? 
            <MenuButton>NEW MESSAGES</MenuButton> : 
            <MenuButton to='/messages'>NEW MESSAGES</MenuButton>
            }
          </View>

          <Route exact path="/atc" component={ATCMenu} />
          <Route exact path="/atc/altituderequest" component={AltitudeRequest} />
          <Route exact path="/atc/routerequest" component={RouteRequest} />
          <Route exact path="/atc/speedrequest" component={SpeedRequest} />
          <Route exact path="/atc/clearancerequest" component={ClearanceRequest} />
          <Route exact path="/atc/expectrequest" component={ExpectRequest} />
          <Route exact path="/atc/voicerequest" component={VoiceRequest} />
          <Route exact path="/atc/logonstatus" component={LogonStatus} />
          <Route exact path="/atc/positionreport" component={PositionReport} />
          <Route exact path="/atc/freetext" component={FreeTextMessage} />

          <Route exact path="/flight" component={FlightMenu} />
          <Route exact path="/flight/departureclearancerequest" component={DepartureClearanceRequest} />
          <Route exact path="/flight/oceanicclearancerequest" component={OceanicClearanceRequest} />
          <Route exact path="/flight/atisrequest" component={AtisRequest} />

          <Route exact path="/review" component={ReviewMenu} />
          <Route exact path="/review/:id" component={ReviewList} />
          <Route exact path="/review/view/:id" component={ReviewView} />

          <Route exact path="/manager" component={ManagerMenu} />
          <Route exact path="/manager/master" component={Master} />
          <Route exact path="/manager/automessage" component={AutoMessages} />
          <Route exact path="/manager/sysinfo" component={SystemInformation} />

          <Route exact path="/messages" component={MessagesMenu} />
          <Route exact path="/messages/view/:id" component={MessageView} />

          <Route exact path="/" component={ExitButton}
          />

          
        </View>

      );

  }

}

class ExitButton extends Component {

  render() {
    return(
      <View id='footer' >
        {/* <View style={{alignContent: 'flex-end'}}> */}
          <button onClick={() => w.close()} style={{marginLeft: 10, marginRight: 10}}>EXIT</button>
        {/* </View> */}
      </View>
    )
  }

}

const mapStateToProps = state => ({
  newMessages: state.cpdlc.newMessages,
  messageHistory: state.cpdlc.messageHistory,
});

export default withRouter(connect(mapStateToProps)(App));
import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

const initialState = {

    callsign: '',
    activeConnection: '',
    inactiveConnection: '',
    logonStatus: -1,
    newMessages: [],
    messageHistory: [],

}

//Action Types
const LOGON_REQUEST = 'FN_CON';
const LOGON_ACCEPT = 'FN_AK';
const CONTROLLER_LOGOFF = 'CONTROLLER_LOGOFF';
const CONTROLLER_TRANSFER = 'CONTROLLER_TRANSFER';
const SEND_MESSAGE = 'SEND_MESSAGE';
const UPDATE_MESSAGE = 'UPDATE_MESSAGE';
const RECEIVE_MESSAGE = 'RECEIVE_MESSAGE';
const RECEIVE_UPDATE = 'RECEIVE_UPDATE';
const VIEW_MESSAGE = 'VIEW_MESSAGE';
const REMOVE_MESSAGE = 'REMOVE_MESSAGE';
const RECEIVE_TRANSFER_REQUEST = 'RECEIVE_TRANSFER_REQUEST';
const RECEIVE_TRANSFER_REJECT = 'RECEIVE_TRANSFER_REJECT';

//Actions
export const logonRequest = values => ({
    type: LOGON_REQUEST,
    payload: values,
    meta: {
        socket: {
            channel: 'logon_request',
        }
    }
});

export const logonAccept = values => ({
    type: LOGON_ACCEPT,
    payload: values,
});

export const controllerLogoff = ({
    type: CONTROLLER_LOGOFF,
});
export const controllerTransfer = ({
    type: CONTROLLER_TRANSFER,
    //payload: values
});

export const sendMessage = values => ({
    type: SEND_MESSAGE,
    payload: {
        message: values,
    },
    meta: {
        socket: {
            channel: 'send_message',
            //room: values.to, //add current data authority here
        },
    },
});
export const updateMessage = values => ({
    type: UPDATE_MESSAGE,
    payload: values,
    meta: {
        socket: {
            channel: 'update_message',
        },
    },
});
export const receiveMessage = values => ({
    type: RECEIVE_MESSAGE,
    payload: {
        message: values
    },
});
export const receiveUpdate = values => ({
    type: RECEIVE_UPDATE,
    payload: values,
});
export const viewMessage = id => ({
    type: VIEW_MESSAGE,
    payload: id,
});

export const removeMessage = id => ({
    type: REMOVE_MESSAGE,
    payload: id,
});
export const receiveTransferRequest = values => ({
    type: RECEIVE_TRANSFER_REQUEST,
    payload: values,
});
export const receiveTransferReject = ({
    type: RECEIVE_TRANSFER_REJECT,
});
//Reducers
export function cpdlc(state = initialState, action) {

    switch(action.type) {

        case LOGON_REQUEST:
            return {
                ...state,
                logonStatus: 0,
                callsign: action.payload.values.flightNumber,
                tailNumber: action.payload.values.tailNumber,
            }

        case LOGON_ACCEPT:
            return {
                ...state,
                logonStatus: 1,
                activeConnection: action.payload,
            }

        case CONTROLLER_LOGOFF:
            return {
                ...state,
                logonStatus: -1,
                activeConnection: '',
            }

        case SEND_MESSAGE:
            return {
                ...state,
                messageHistory: [...state.messageHistory, action.payload.message]
            }

        case RECEIVE_MESSAGE:
            return {
                ...state,
                newMessages: [...state.newMessages, action.payload.message]
            }

        case RECEIVE_UPDATE:
            var arr = state.messageHistory;

            return{

                ...state,
                messageHistory: arr.map((item) => {
                    if(item.id !== action.payload.id) {
                        return item;
                    }

                    return {
                        ...item,
                        ...action.payload
                    }
                })

            }

        case UPDATE_MESSAGE:

            arr = state.newMessages;

            return{
                ...state,
                newMessages: arr.map((item) => {
                    if(item.id !== action.payload.id) {
                        return item;
                    }

                    return {
                        ...item,
                        ...action.payload
                    }
                })
            }
            
            // var thisMessage = state.newMessages.find(function(message) {
            //     return message.id === action.payload;
            // })

            // if(!thisMessage) {
            //     return state;
            // }

            // return Object.assign({}, state, {
            //     newMessages: [...state.newMessages, thisMessage]
            // });
        
        /**Marks a New Message as viewed by moving it from newMessages to messageHistory
         * If a response is required, this should be done after the response is sent
        */
        case VIEW_MESSAGE:

            var thisMessage = state.newMessages.find(function(message) {
                return message.id === action.payload;
            })
            if(!thisMessage) {
                return state;
            }

            //Add Viewed Message to messageHistory
            return Object.assign({}, state, {
                messageHistory: [...state.messageHistory, thisMessage]
            });

        case REMOVE_MESSAGE:

            thisMessage = state.newMessages.find(function(message) {
                return message.id === action.payload;
            })
            if(!thisMessage) {
                return state;
            }

            //Remove Viewed Message from newMessages
            return Object.assign({}, state, {
                newMessages: [
                    ...(state.newMessages.filter(message => (message.id !== thisMessage.id))),
                ]
            });

        case RECEIVE_TRANSFER_REQUEST:
            return {
                ...state,
                inactiveConnection: action.payload
            }

        case RECEIVE_TRANSFER_REJECT:
            return {
                ...state,
                inactiveConnection: ''
            }

        case CONTROLLER_TRANSFER:
            return {
                ...state,
                activeConnection: state.inactiveConnection,
                inactiveConnection: ''
            }

        default:
            return state;

    }

}

const reducers = {
    form: formReducer,
    cpdlc: cpdlc,
}

const allReducers = combineReducers(reducers);
export default allReducers;
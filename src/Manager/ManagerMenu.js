import React from 'react';
import {View} from 'react-desktop/windows';
import '../App.css';

import SubMenuButton from '../SubMenuButton';

const views = [
    {id: '', label: 'ACARS'},
    {id: '', label: 'VHF'},
    {id: '', label: 'SATCOM'},
    {id: '', label: 'ADS'},
    {id: '', label: 'HF'},

    {id: 'sysinfo', label: 'SYSTEM INFO'},
    {id: '', label: 'PRINTER'},
    {id: 'automessage', label: 'AUTOMATIC MESSAGES'},
    {id: 'master', label: 'MASTER'},
]

export const ManagerMenu = () => {

    return(
            
        <View style={{flexDirection: 'column', flex: 1,}}>

            <View style={{color: 'white', alignSelf: 'center', fontSize: 30, paddingBottom: 10}}>MANAGER</View>

            <View style={{flexDirection: 'row', color: 'white', width: '100%', height: '100%', fontSize: 25, }}>
            
                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                    {views.slice(0, 5).map(({id, label}) => {
                        if(id)
                            return <SubMenuButton to={'/manager/'+id}>{label}</SubMenuButton>
                        else
                            return <SubMenuButton>{label}</SubMenuButton>
                    })}
                </View>

                <View style={{flex: 1, flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'center', padding: '20px'}}>
                    {views.slice(5, 9).map(({id, label}) => {
                        if(id)
                            return <SubMenuButton to={'/manager/'+id}>{label}</SubMenuButton>
                        else
                            return <SubMenuButton>{label}</SubMenuButton>
                    })}
                </View>              
            
            </View>

        </View>

    )

}

export default ManagerMenu;
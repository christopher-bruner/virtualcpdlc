import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import {ExitButtonBar} from '../ButtonBar';
import MasterForm from '../Forms/MasterForm';

class Master extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column',height: '100%', justifyContent: 'space-between'}}>
                <Header label="MASTER" />
                <MasterForm />
                <ExitButtonBar />
            </View>

        )
    }
}

export default Master;
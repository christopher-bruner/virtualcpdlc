import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import {ExitButtonBar} from '../ButtonBar';
import Header from '../Header';

const remote = window.require('electron').remote;
let w = remote.getCurrentWindow();

class SystemInformation extends Component {

    render() {
        return(

            <View className='Root' style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label="SYSTEM INFORMATION" />
                
                <ExitButtonBar />
            </View>
        )
    }
}

export default SystemInformation;
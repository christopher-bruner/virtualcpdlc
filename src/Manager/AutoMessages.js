import React, {Component} from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import AutoMessagesForm from '../Forms/AutoMessagesForm';
import {ExitButtonBar} from '../ButtonBar';

class AutoMessages extends Component {

    render() {
        return(

            <View style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label="AUTOMATIC MESSAGES" />
                <AutoMessagesForm />
                <ExitButtonBar />
            </View>
        )
    }
}

export default AutoMessages;
import React from 'react';
import '../App.css';
import {View} from 'react-desktop/windows';
import Header from '../Header';
import {NewMessageListItem} from '../Message';
import {ExitButtonBar} from '../ButtonBar';
import {connect} from 'react-redux';

const MessagesMenu = (props) => {

    const {
        messageList
    } = props;

    return(

        <View style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
            <Header label="NEW MESSAGES" />
            <ul>
            {
                messageList.map(({id, time, messageType, args}) => {
                 return <NewMessageListItem to={'/messages/view/'+id} time={time} messageType={messageType} args={args} />
                })
            }
            </ul>
            <ExitButtonBar />
        </View>

    )

}

const mapStateToProps = state => ({
    messageList: state.cpdlc.newMessages,
});

export default connect(mapStateToProps)(MessagesMenu);
import React, {Component} from 'react';
import '../App.css';
import {getMessage, parseMessageLabel, parseMessage, parseResponseAttribute} from '../Message';
import {View} from 'react-desktop/windows';
import Header from '../Header';

import {store} from '../index';
import {viewMessage, removeMessage} from '../reducer';
import {connect} from 'react-redux';
import { UMMessageMap, DMMessageMap } from '../MessageDefinitions';

const sendViewMessage = id => {
    store.dispatch(viewMessage(id));
    store.dispatch(removeMessage(id));
}

class MessageView extends Component {

    msg = getMessage(this.props.newMessages, this.props.match.params.id);

    componentDidMount() {

        let el;
        el = UMMessageMap.get(this.msg.messageType);
        if(!el) {
            el = DMMessageMap.get(this.msg.messageType);
            if(!el) {
                return;
            }
        }

        if(el.responseAttribute === 'N') {
            sendViewMessage(this.msg.id);
        }

    }

    render() {
        
        // const {
        //     match,
        // } = this.props;

        return(
            
            <View style={{flexDirection: 'column', flex: 1, height: '100%', justifyContent: 'space-between'}}>
                <Header label={parseMessageLabel(this.msg.messageType)} />
                <View style={{marginLeft: '25%', marginTop: '50px', color: 'white', fontSize: 27}}>
                    {parseMessage(this.msg.messageType, this.msg.args)}
                </View>
                {parseResponseAttribute(this.msg)}
            </View>

        )

    }

}

const mapStateToProps = state => ({
    newMessages: state.cpdlc.newMessages,
});

export default connect(mapStateToProps)(MessageView);
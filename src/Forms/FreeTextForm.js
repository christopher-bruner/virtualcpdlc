import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import FreeTextFormSection from './FreeTextFormSection';
import {reduxForm, Form} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';

const sendFreeText = values => {

    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    if(values.freetext) {
        message.messageType = 'DM67';
        message.args = values.freetext;
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class FreeTextForm extends Component {

    render() {
        
        const {handleSubmit} = this.props;

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 50, flexDirection: 'column', marginLeft: 25}}>
                <FreeTextFormSection />
            </View>
            </Form>

        )
    }

}

export default reduxForm({form: 'freetextForm', onSubmit: sendFreeText, destroyOnUnmount: false})(FreeTextForm);
import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';
import { isNumber } from 'util';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendPositionReport(values) {

    await sleep(1000);

    if(!values.position || !values.actualtime || !values.speed || !values.altitude || 
        !values.estfix || !values.estimatedtime || !values.next) {
            throw new SubmissionError({
                _error: 'INVALID ENTRY'
            });
    }
    if(!isNumber(values.actualtime) || !isNumber(values.estimatedtime) || !isNumber(values.altitude)) {
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    
    let message = {};

    message.time = createTimeStamp();
    message.messageType = 'DM48';
    message.args = [];

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    message.args.push('REPORTING '+values.position+' AT '+values.actualtime);
    message.args.push('SPEED '+values.speed+' ALTITUDE '+values.altitude);
    message.args.push('ESTIMATING '+values.estfix+' AT '+values.estimatedtime);
    message.args.push('NEXT '+values.next);

    if(values.temperature){
        message.args.push('TEMP '+values.temperature);
    }
    if(values.wind){
        message.args.push('WIND '+values.wind);
    }
    if(values.fuel){
        message.args.push('FUEL: '+values.fuel);
    }
    if(values.turbulence){
        message.args.push(values.turbulence+' TURBULENCE');
    }
    if(values.icing){
        message.args.push(values.icing+' ICING');
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class PositionReportForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'row'}}>
            
            <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 30}}>

                <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, }}>
                    POS: <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='position' component='input' maxlength='6' type='text' />
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50, color: 'white', fontSize: 25,}}>
                    ALTITUDE: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='altitude' component='input' maxlength='5' type='text' />
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, }}>
                    EST: <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='estfix' component='input' maxlength='4' type='text' />
                </View>

                <View style={{flexDirection: 'row', marginBottom: 95, color: 'white', fontSize: 25,}}>
                    NEXT: <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='next' component='input' maxlength='5' type='text' />
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 40, color: 'white', fontSize: 25,}}>
                    SPEED: <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='speed' component='input' maxlength='6' type='text' />
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25,}}>
                    POS FUEL: <Field disabled={submitting} style={{width: '75px', marginLeft: 10, marginRight: 10}} name='fuel' component='input' maxlength='6' type='text' />X 1000
                </View>

                <View style={{marginBottom: 30, flexDirection: 'column', color: 'white', fontSize: 25, }}>

                    TURBULENCE:
                    <label className="container" style={{marginTop: 45}}>LIGHT
                        <Field disabled={submitting} component='input' type='radio' name='turbulence' value='light' />
                        <span className="radio"></span>
                    </label>
                    <label className="container" style={{marginTop: 15}}>MODERATE
                        <Field disabled={submitting} component='input' type='radio' name='turbulence' value='moderate' />
                        <span className="radio"></span>
                    </label>
                    <label className="container" style={{marginTop: 15}}>SEVERE
                        <Field disabled={submitting} component='input' type='radio' name='turbulence' value='severe' />
                        <span className="radio"></span>
                    </label>

                </View>

            </View>

            <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 120}}>

                <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25,}}>
                    ATA: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='actualtime' component='input' maxlength='4' type='text' />Z
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, marginTop: 75, color: 'white', fontSize: 25,}}>
                    ETA: <Field disabled={submitting} style={{width: '60px'}} name='estimatedtime' component='input' maxlength='4' type='text' />Z
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, marginTop: 65, marginLeft: 10, color: 'white', fontSize: 25,}}>
                    DEST ETA: <Field disabled={submitting} style={{width: '60px'}} name='destinationeta' component='input' maxlength='4' type='text' />Z
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 65, color: 'white', fontSize: 25,}}>
                    TEMP: <Field disabled={submitting} style={{width: '50px', marginLeft: 10}} name='temperature' component='input' maxlength='4' type='text' />C
                </View>

                <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 60, color: 'white', fontSize: 25,}}>
                    WIND: <Field disabled={submitting} style={{width: '75px'}} name='wind' component='input' maxlength='7' type='text' />KT
                </View>

                <View style={{marginBottom: 30, flexDirection: 'column', color: 'white', fontSize: 25,}}>

                    ICING:
                    <label className="container" style={{marginTop: 45}}>TRACE
                        <Field disabled={submitting} component='input' type='radio' name='icing' value='trace' />
                        <span className="radio"></span>
                    </label>
                    <label className="container" style={{marginTop: 15}}>LIGHT
                        <Field disabled={submitting} component='input' type='radio' name='icing' value='light' />
                        <span className="radio"></span>
                    </label>
                    <label className="container" style={{marginTop: 15}}>MODERATE
                        <Field disabled={submitting} component='input' type='radio' name='icing' value='moderate' />
                        <span className="radio"></span>
                    </label>
                    <label className="container" style={{marginTop: 15}}>SEVERE
                        <Field disabled={submitting} component='input' type='radio' name='icing' value='severe' />
                        <span className="radio"></span>
                    </label>

                </View>

            </View>

        </View>

        {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
        {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='positionreportForm' />}

        </Form>
        
        )
    }

}

export default reduxForm({form: 'positionreportForm', onSubmit: sendPositionReport, destroyOnUnmount: false})(PositionReportForm);
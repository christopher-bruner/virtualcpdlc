import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import FreeTextFormSection from './FreeTextFormSection';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {connect} from 'react-redux';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendDepartureClearanceRequest(values) {

    await sleep(1000);

    if(!values.flightNumber || !values.departure || !values.arrival || !values.facility){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();
    message.messageType = 'B3';

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    message.args = [values.departure+' TO '+values.destination];
    if(values.gate){
        message.args.push('GATE '+values.gate);
    }
    if(values.atis){
        message.args.push('ATIS '+values.atis);
    }
    if(values.freetext){
        message.args.push(values.freetext);
    }
    
    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
    console.log(store.getState());
}

class DepartureClearanceRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        // const initData = {
        //     "flightNumber": this.props.flightNumber,
        //     "facility": this.props.activeCenter,
        // }

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 25}}>
                
                <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 30}}>
                
                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, marginRight: 10}}>
                        FLT NUMBER: <Field disabled={submitting} style={{width: '85px', marginLeft: 10}} name='flightNumber' component='input' maxlength='8' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, marginRight: 10}}>
                        DEPARTURE: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='departure' component='input' maxlength='4' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50, color: 'white', fontSize: 25, marginRight: 10}}>
                        ATIS: <Field disabled={submitting} style={{width: '25px', marginLeft: 10}} name='atis' component='input' maxlength='1' type='text' />
                    </View>

                    <View style={{flex: 1,flexDirection: 'row', marginBottom: 30, marginLeft: 25}}>
                        <FreeTextFormSection />
                    </View>

                </View>

                <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 30}}>

                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, marginRight: 10}}>
                        FACILITY: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='facility' component='input' maxlength='4' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25, marginRight: 10}}>
                        DESTINATION: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='destination' component='input' maxlength='4' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50, color: 'white', fontSize: 25, marginRight: 10}}>
                        GATE: <Field disabled={submitting} style={{width: '50px', marginLeft: 10}} name='gate' component='input' maxlength='5' type='text' />
                    </View>

                </View>
                
            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='departureclearancerequestForm' />}

            </Form>

        )

    }

}

const mapStateToProps = state => ({
    activeCenter: state.cpdlc.activeConnection,
    flightNumber: state.cpdlc.flightNumber,
});

DepartureClearanceRequestForm = reduxForm({form: 'departureclearancerequestForm', onSubmit: sendDepartureClearanceRequest, destroyOnUnmount: false, 
    })(DepartureClearanceRequestForm);
export default connect(mapStateToProps)(DepartureClearanceRequestForm);

import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import FreeTextFormSection from './FreeTextFormSection';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';
import { isNumber } from 'util';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendOceanicClearanceRequest(values) {

    await sleep(1000);

    if(!values.flightNumber || !values.facility || !values.entrypoint || !values.eta || !values.flightlevel || !values.mach){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(!isNumber(values.eta) || !isNumber(values.flightlevel) || !isNumber(values.mach)) {
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();
    message.messageType = 'B1';

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    message.args = [values.flightNumber];
    message.args.push('ENTRY POINT '+values.entrypoint+' ETA '+values.eta);
    message.args.push(values.flightlevel+' MACH '+values.mach);

    if(values.freetext){
        message.args.push(values.freetext);
    }

    message.id = createID(message);
    message.response = undefined;
    
    store.dispatch(sendMessage(message));
}

class OceanicClearanceRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 25}}>
                
                <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 30}}>
                
                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25,}}>
                        FLT NUMBER: <Field disabled={submitting} style={{width: '85px', marginLeft: 10}} name='flightNumber' component='input' maxlength='8' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, color: 'white', fontSize: 25,}}>
                        ENTRY POINT: <Field disabled={submitting} style={{width: '100px', marginLeft: 10}} name='entrypoint' component='input' maxlength='6' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50, color: 'white', fontSize: 25,}}>
                        ETA: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='eta' component='input' maxlength='4' type='text' />Z
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50}}>
                        <FreeTextFormSection />
                    </View>

                </View>

                <View style={{flex: 1, height: '100%', flexDirection: 'column', marginTop: 70, marginLeft: 30}}>

                    <View style={{flexDirection: 'row', color: 'white', fontSize: 25, marginBottom: 30}}>
                        ATC FACILITY: <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='facility' component='input' maxlength='4' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', color: 'white', fontSize: 25, marginBottom: 30}}>
                        FLIGHT LEVEL: <Field disabled={submitting} style={{width: '60px'}} name='flightlevel' component='input' maxlength='5' type='text' />
                    </View>

                    <View style={{flexDirection: 'row', marginBottom: 30, marginLeft: 50, color: 'white', fontSize: 25,}}>
                        MACH: <Field disabled={submitting} style={{width: '40px'}} name='mach' component='input' maxlength='3' type='text' />
                    </View>

                </View>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='oceanicclearancerequestForm' />}

            </Form>

        )

    }
}

export default reduxForm({form: 'oceanicclearancerequestForm', onSubmit: sendOceanicClearanceRequest, destroyOnUnmount: false})(OceanicClearanceRequestForm);
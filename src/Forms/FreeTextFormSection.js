import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {Field} from 'redux-form';

class FreeTextFormSection extends Component {

    render() {
        return(

            <View style={{color: 'white', fontSize: 25,}}>
                FREE TEXT: <Field name='freetext' component='textarea' rows='4' cols='30' style={{marginLeft: 10}}/>
            </View>

        )
    }

}

export default FreeTextFormSection;
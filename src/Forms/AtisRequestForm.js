import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendAtisRequest(values) {

    await sleep(1000);

    if(!values.atisrequest){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.atisrequest && !values.airport){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();
    message.to = "AOC";
    message.from = store.getState().cpdlc.callsign;

    message.args = values.airport;
    message.messageType = '5D';
    message.id = createID(message);
    message.response = -1;

    store.dispatch(sendMessage(message));
}

class AtisRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 25}}>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginTop: 25, marginLeft: 25}}>AIRPORT: 
                    <Field disabled={submitting} style={{marginLeft: 20}} component='input' type='checkbox' name='atisrequest' />
                    <span className="checkmark"></span>
                    <Field disabled={submitting} style={{width: '60px'}} name='airport' component='input' maxlength='4' type='text' />
                </label>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='atisrequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'atisrequestForm', onSubmit: sendAtisRequest, destroyOnUnmount: false})(AtisRequestForm);
import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';
import { isNumber } from 'util';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendRouteRequest(values) {

    await sleep(1000);

    if(!values.routerequesttype){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.routerequesttype === 'direct' && !values.directto){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if((values.routerequesttype === 'heading' && !values.heading) || !isNumber(values.heading)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if((values.routerequesttype === 'track' && !values.track) || !isNumber(values.track)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.routerequesttype === 'deparr' && !values.deparr){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if((values.routerequesttype === 'deviation' && !values.deviation) || !isNumber(values.deviation)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if((values.routerequesttype === 'offset' && !values.offset) || !isNumber(values.offset)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    
    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    switch(values.routerequesttype) {

        case 'direct':
            message.messageType = 'DM22';
            message.args = values.directto;
            break;

        case 'heading':
            message.messageType = 'DM70';
            message.args = values.heading;
            break;

        case 'track':
            message.messageType = 'DM71';
            message.args = values.track;
            break;
        case 'deparr':
            message.messageType = 'DM24';
            message.args = values.deparr.split('.');
            break;

        case 'deviation':
            message.messageType = 'DM27';
            message.args = values.deviation;
            break;

        case 'offset':
            message.messageType = 'DM15';
            message.args = [message.offset];
            if(message.offsetat) {
                message.args.push('AT '+message.offsetat);
            }
            break;

        default:
            return;
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class RouteRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(
            
            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'column', marginTop: 25}}>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30}}>DIRECT TO: 
                    <Field disabled={submitting} component='input' type='radio' name='routerequesttype' value='direct' />
                    <span className="radio"></span>
                    <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='directto' component="input" maxlength='6' type="text" />
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30, marginTop: 25}}>HEADING: 
                    <Field disabled={submitting}  component='input' type='radio' name='routerequesttype' value='heading' />
                    <span className="radio"></span>
                    <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='heading' component="input" maxlength='3' type="text" />
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30, marginTop: 25}}>TRACK: 
                    <Field disabled={submitting} component='input' type='radio' name='routerequesttype' value='track' />
                    <span className="radio"></span>
                    <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='track' component="input" maxlength='3' type="text" />
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30, marginTop: 25}}>DEP/ARR: 
                    <Field disabled={submitting} component='input' type='radio' name='routerequesttype' value='deparr' />
                    <span className="radio"></span>
                    <Field disabled={submitting}  style={{width: '120px', marginLeft: 10}} name='deparr' component="input" maxlength='7' type="text" />
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30, marginTop: 25}}>WEATHER DEVIATION UP TO: 
                    <Field disabled={submitting} component='input' type='radio' name='routerequesttype' value='deviation' />
                    <span className="radio"></span>
                    <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='deviation' component="input" maxlength='4' type="text" />NM
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 30, marginTop: 25}}>OFFSET: 
                    <Field disabled={submitting}  component='input' type='radio' name='routerequesttype' value='offset' />
                    <span className="radio"></span>
                    <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='offset' component="input" maxlength='4' type="text" />NM
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 70, marginTop: 25}}>OFFSET AT: 
                    <Field disabled={submitting}  component='input' type='checkbox' name='offset' />
                    <span className="checkmark"></span>
                    <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='offsetat' component="input" maxlength='6' type="text" />
                </label>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='routerequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'routerequestForm', onSubmit: sendRouteRequest, destroyOnUnmount: false})(RouteRequestForm);
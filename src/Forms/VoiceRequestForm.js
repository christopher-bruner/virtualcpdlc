import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendVoiceRequest(values) {

    await sleep(1000);

    if(!values.voicerequest){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    if(values.voicerequest) {
        message.messageType = 'DM20';
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class VoiceRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;
        
        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 25, marginLeft: 25}}>
                
                <label className="container" style={{flex: 1, flexDirection: 'row', marginTop: 25,}}>REQUEST VOICE CONTACT
                    <Field disabled={submitting} style={{marginLeft: 20}} component='input' type='checkbox' name='voicerequest' />
                    <span className="checkmark"></span>
                </label>
                
            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='voicerequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'voicerequestForm', onSubmit: sendVoiceRequest, destroyOnUnmount: false})(VoiceRequestForm);
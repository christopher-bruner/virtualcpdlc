import React from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';

let AutoMessagesForm = props => {

    const {
        automessagesoffChecked,
        handleSubmit
    } = props;

    return(

        <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 50, marginLeft: 25}}>
        
                <label style={{flex: 1}} class="container">AUTO MESSAGES OFF
                    <Field component='input' type='checkbox' name='automessagesoff' id='automessagesoff' />
                    <span class="checkmark"></span>
                </label>

                {automessagesoffChecked && (
                    <label style={{flex: 1}} class="container">CONFIRM OFF
                        <Field component='input' type='checkbox' name='confirmreset' id='confirmreset' />
                        <span class="checkmark"></span>
                    </label>)}

            </View>
        </Form>

        )

}

AutoMessagesForm = reduxForm({form: 'automessagesForm',})(AutoMessagesForm);

const selector = formValueSelector('automessagesForm');

AutoMessagesForm = connect(state => {
    const automessagesoffChecked = selector(state, 'automessagesoff')
    return {automessagesoffChecked}
  })(AutoMessagesForm)

export default AutoMessagesForm;
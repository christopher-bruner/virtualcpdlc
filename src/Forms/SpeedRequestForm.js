import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';
import { isNumber } from 'util';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendSpeedRequest(values) {

    await sleep(1000);

    if(!values.speedrequest){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if((values.speedrequest && !values.speed) ||  !isNumber(values.speed)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    if(values.speedrequest) {
        message.messageType = 'DM18';
        message.args = values.speed;
    } else {
        return;
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class SpeedRequestForm extends Component {
    
    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(
          
            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'column', marginTop: 25, marginLeft: 25}}>
            
            <label className="container" style={{flexDirection: 'row'}}>SPEED: 
                <Field disabled={submitting} component='input' type='checkbox' name='speedrequest' id='speedrequest' />
                <span className="checkmark"></span>
                <Field disabled={submitting} style={{width: '40px', marginLeft: 10}} name='speed' component="input" maxlength='6' type="text" />
            </label>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='speedrequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'speedrequestForm', onSubmit: sendSpeedRequest, destroyOnUnmount: false})(SpeedRequestForm);
import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {logonRequest} from '../reducer';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendLogonRequest(values) {

    await sleep(1000);

    if(!values.logonTo || !values.flightNumber || !values.tailNumber) {
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    store.dispatch(logonRequest({values}));
}

class LogonForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;

        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flexDirection: 'column', flex: 1, height: '100%', }}>

                <View style={{flexDirection: 'row', color: 'white', fontSize: 25, marginLeft: 45, marginRight: 10, marginTop: 25, paddingLeft: 45}}>
                    MAX UPLINK DELAY: <Field disabled={submitting} style={{marginLeft: 10, marginRight: 3, width: '40px',}} name='uplinkDelay' component="input" maxlength='3' type="text" />SEC
                </View>

                <View style={{flexDirection: 'column', color: 'white', fontSize: 25, flex: 1, marginTop: 35, paddingLeft: 20, paddingBottom: 20,}}>
                    <View style={{flexDirection: 'row', paddingLeft: 45, marginTop: 25, flexWrap: 'wrap',}}>
                        LOGON TO: <Field disabled={submitting} style={{width: '60px', marginLeft: 10,}} name='logonTo' component="input" maxlength='4' type="text" />
                    </View>
                    <View style={{flexDirection: 'row', color: 'white', fontSize: 25, marginTop: 25, paddingLeft: 45, flexWrap: 'wrap', marginRight: 10,}}>
                        FLIGHT NUMBER: <Field disabled={submitting} style={{width: '85px'}} name='flightNumber' component="input" maxlength='8' type="text" />
                    </View>
                    <View style={{flexDirection: 'row', color: 'white', fontSize: 25, marginTop: 25, paddingLeft: 45, flexWrap: 'wrap', marginRight: 10,}}>
                        TAIL NUMBER: <Field disabled={submitting} style={{width: '75px'}} name='tailNumber' component="input" maxlength='8' type="text" />
                    </View>
                </View>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' /> : <ButtonBar formName={'logonForm'} />}

            </Form>

        )

    }

}

export default reduxForm({form: 'logonForm', onSubmit: sendLogonRequest, destroyOnUnmount: false})(LogonForm);
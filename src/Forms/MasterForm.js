import React from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';

let MasterForm = props => {

    const {
        systemresetChecked,
        handleSubmit
    } = props;

    return(

        <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 50, marginLeft: 25}}>
        
                <label style={{flex: 1}} class="container">DATA LINK SYSTEM RESET
                    <Field component='input' type='checkbox' name='systemreset' id='systemreset' />
                    <span class="checkmark"></span>
                </label>

                {systemresetChecked && (
                <label style={{flex: 1}} class="container">CONFIRM RESET
                    <Field component='input' type='checkbox' name='confirmreset' id='confirmreset' />
                    <span class="checkmark"></span>
                </label>)}

            </View>
        </Form>

        )

}

MasterForm = reduxForm({form: 'masterForm',})(MasterForm);

const selector = formValueSelector('masterForm');

MasterForm = connect(state => {
    const systemresetChecked = selector(state, 'systemreset')
    return {systemresetChecked}
  })(MasterForm)

export default MasterForm;
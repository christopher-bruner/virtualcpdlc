import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendClearanceRequest(values) {

    await sleep(1000);

    if(!values.clearancerequest){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    if(values.clearancerequest) {
        message.messageType = 'DM25';
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class ClearanceRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;
        
        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, marginTop: 25, marginLeft: 25}}>
                
                <label className="container" style={{flex: 1, flexDirection: 'row', marginTop: 25,}}>REQUEST CLEARANCE
                    <Field disabled={submitting} style={{marginLeft: 20}} component='input' type='checkbox' name='clearancerequest' />
                    <span className="checkmark"></span>
                </label>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' />: <ButtonBar formName='clearancerequestForm' />}

            </Form>


        )

    }

}

export default reduxForm({form: 'clearancerequestForm', onSubmit: sendClearanceRequest, destroyOnUnmount: false})(ClearanceRequestForm);
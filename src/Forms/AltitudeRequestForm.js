import React, {Component} from 'react';
import {View, Text} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import FreeTextFormSection from './FreeTextFormSection';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';
import { isNumber } from 'util';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendAltitudeRequest(values) {

    await sleep(1000);

    if(!values.altituderequesttype) {
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.altituderequesttype === 'altitude' && !values.requestedaltitude && isNumber(values.requestedaltitude)){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.altituderequesttype === 'block ' && !(values.block1 && values.block2) && !(isNumber(values.block1) && isNumber(values.block2)) ){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.climbtype === 'step' && !values.stepat){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();
    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    switch (values.altituderequesttype) {
        case 'altitude':
            message.messageType = 'DM6';
            message.args = [values.requestedaltitude];
            break;
        case 'block':
            message.messageType = 'DM7';
            message.args = [values.block1, values.block2];
            break;
        case 'requestdescent':
            message.messageType = 'DM10';
            message.args = [];
            break;
        default:
            return;
    }
    if (values.climbtype === 'cruiseclimb') {
        message.messageType = 'DM9';
    }
    else if (values.climbtype === 'step') {
        message.args.push(values.stepat);
    }
    if (values.atpilotsdiscretion) {
        message.args.push('At Pilots Discretion');
    }
    if (values.duetoweather) {
        message.args.push('Due to Weather');
    }
    if (values.duetoaircraftperformance) {
        message.args.push('Due to Aircraft Performance');
    }
    if (values.freetext) {
        message.args.push(values.freetext);
    }

    message.id = createID(message);
    message.response = undefined;
    store.dispatch(sendMessage(message));

}

class AltitudeRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;
        
        return(

            <Form onSubmit={handleSubmit}>
            <View style={{flex: 1, flexDirection: 'column'}}>

                <View style={{flex: 1, flexDirection: 'column', marginTop: 25, marginLeft: 25}}>

                    <label className="container" style={{flexDirection: 'row', marginBottom: 25}}>ALTITUDE
                        <Field disabled={submitting} component='input' type='radio' name='altituderequesttype' value='altitude' />
                        <span className="radio"></span>
                        <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='requestedaltitude' component="input" maxlength='5' type="text" />
                    </label>

                    <label className="container" style={{flexDirection: 'row', marginLeft: 40,}}>STEP AT: 
                        <Field disabled={submitting} component='input' type='radio' name='climbtype' value='step' />
                        <span className="radio"></span>
                        <Field disabled={submitting} style={{width: '75px', marginLeft: 10}} name='stepat' component="input" maxlength='6' type="text" />
                    </label>

                    <label className="container" style={{flexDirection: 'row', marginLeft: 40, marginTop: 15,}}>CRUISE CLIMB
                        <Field disabled={submitting} component='input' type='radio' name='climbtype' value='cruiseclimb' />
                        <span className="radio"></span>
                    </label>

                </View>

                <View style={{flex: 1, flexDirection: 'column', marginTop: 25, marginLeft: 25}}>

                    <label className="container" style={{flexDirection: 'row'}}>BLOCK: 
                        <Field disabled={submitting} component='input' type='radio' name='altituderequesttype' value='block' />
                        <span className="radio"></span>
                        <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='block1' component="input" maxlength='5' type="text" />
                    </label>
                    <View style={{flexDirection: 'row', marginLeft: 70,}}>
                        <Text style={{color: 'white', fontSize: 25, marginRight: 8}}>TO: </Text>
                        <Field disabled={submitting} style={{width: '60px', marginLeft: 10}} name='block2' component="input" maxlength='5' type="text" />
                    </View>

                </View>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginTop: 25, marginLeft: 25}}>REQUEST DESCENT
                    <Field disabled={submitting} component='input' type='radio' name='altituderequesttype' value='requestdescent' />
                    <span className="radio"></span>
                </label>

                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 25, marginTop: 10,}}>AT PILOTS DISCRETION
                    <Field disabled={submitting} component='input' type='checkbox' name='atpilotsdiscretion' id='atpilotsdiscretion' />
                    <span className="checkmark"></span>
                </label>
                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 25, marginTop: 10,}}>DUE TO WEATHER
                    <Field disabled={submitting} component='input' type='checkbox' name='duetoweather' id='duetoweather' />
                    <span className="checkmark"></span>
                </label>
                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 25, marginTop: 10,}}>DUE TO AIRCRAFT PERFORMANCE
                    <Field disabled={submitting} component='input' type='checkbox' name='duetoaircraftperformance' id='duetoaircraftperformance' />
                    <span className="checkmark"></span>
                </label>
                <label className="container" style={{flex: 1, flexDirection: 'row', marginLeft: 25, marginTop: 10,}}>MAINTAIN OWN SEPARATION AND VMC
                    <Field disabled={submitting} component='input' type='checkbox' name='maintainownseparation' id='maintainownseparation' />
                    <span className="checkmark"></span>
                </label>

                <View style={{flex: 1, flexDirection: 'row', marginTop: 25, marginLeft: 10}}>
                    <FreeTextFormSection />
                </View>

            </View>

            {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
            {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' /> : <ButtonBar formName='altituderequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'altituderequestForm', onSubmit: sendAltitudeRequest, destroyOnUnmount: false,})(AltitudeRequestForm);
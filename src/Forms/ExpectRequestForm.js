import React, {Component} from 'react';
import {View} from 'react-desktop/windows';
import {reduxForm, Field, Form, SubmissionError} from 'redux-form';
import FreeTextFormSection from './FreeTextFormSection';
import {store} from '../index';
import {sendMessage} from '../reducer';
import {createTimeStamp, createID} from '../Message';
import ButtonBar, {StatusButtonBar} from '../ButtonBar';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
async function sendExpectRequest(values) {

    await sleep(1000);

    if(!values.altitudrequesttype) {
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.altitudrequesttype === 'altitude' && !values.requestedaltitude){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }
    if(values.requestedspeed && !values.speed){
        throw new SubmissionError({
            _error: 'INVALID ENTRY'
        });
    }

    let message = {};

    message.time = createTimeStamp();

    message.to = store.getState().cpdlc.activeConnection;
    message.from = store.getState().cpdlc.callsign;

    if(values.altitudrequesttype){

        switch(values.altitudrequesttype){

            case 'altitude':
                if(values.cruiseclimb){
                    message.messageType = 'DM54';
                    message.args = [values.requestedaltitude];
                } else {
                    message.messageType = 'DM49';
                    message.args = ['ALTITUDE '+values.requstedaltitude];
                }
                break;
            
            case 'higheraltitude':
                message.messageType = 'DM53';
                message.args = [];
                break;

            case 'loweraltitude':
                message.messageType = 'DM52';
                message.args = [];
                break;

            default: return;

        }

    } else if(values.requestedspeed){
        message.messageType = 'DM49';
        message.args = ['SPEED '+values.speed];
    } else if(values.backonroute) {
        message.messageType = 'DM51';
        message.args = [];
    }

    if(values.freetext) {
        message.args.push(values.freetext);
    }

    message.id = createID(message);
    message.response = undefined;

    store.dispatch(sendMessage(message));
}

class ExpectRequestForm extends Component {

    render() {

        const {handleSubmit, submitting, error, pristine} = this.props;
        
        return(

                <Form onSubmit={handleSubmit}>
                <View style={{flex: 1, marginTop: 50, flexDirection: 'column', marginLeft: 25}}>

                    <label className="container" style={{flexDirection: 'row', marginTop: 25}}>ALTITUDE
                        <Field disabled={submitting}  component='input' type='radio' name='altituderequesttype' value='altitude' />
                        <span className="radio"></span>
                        <Field disabled={submitting}  style={{width: '60px', marginLeft: 10}} name='requestedaltitude' maxlength='5' component="input" type="text" />
                    </label>

                    <label className="container" style={{flexDirection: 'row', marginLeft: 40, marginTop: 25}}>CRUISE CLIMB
                        <Field disabled={submitting}  component='input' type='checkbox' name='cruiseclimb' />
                        <span className="checkmark"></span>
                    </label>

                    <label className="container" style={{flexDirection: 'row', marginTop: 25}}>HIGHER ALT
                        <Field disabled={submitting}  component='input' type='radio' name='altituderequesttype' value='higheraltitude' />
                        <span className="radio"></span>
                    </label>

                    <label className="container" style={{flexDirection: 'row', marginTop: 25}}>LOWER ALT: 
                        <Field disabled={submitting}  component='input' type='radio' name='altituderequesttype' value='loweraltitude' />
                        <span className="radio"></span>
                    </label>

                </View>

                <label className="container" style={{flex: 1, height: '100%', flexDirection: 'row', marginLeft: 25, marginTop: 25}}>SPEED: 
                    <Field disabled={submitting}  component='input' type='checkbox' name='requestedspeed' style={{marginLeft: 20}} />
                    <span className="checkmark"></span>
                    <Field disabled={submitting}  name='speed' component='input' maxlength='6' type='text' style={{width: '40px'}} />
                </label>

                <label className="container" style={{flex: 1, height: '100%', flexDirection: 'row', marginLeft: 25, marginTop: 25}}>BACK ON ROUTE
                    <Field disabled={submitting}  component='input' type='checkbox' name='backonroute' style={{marginLeft: 20}} />
                    <span className="checkmark"></span>
                </label>

                <View style={{flex: 1, height: '100%', flexDirection: 'row', marginLeft: 50, marginTop: 40}}>
                    <FreeTextFormSection />
                </View>

                {error && !pristine ? <StatusButtonBar statusLabel={error} /> : ''}
                {submitting && !pristine ? <StatusButtonBar statusLabel='SUBMITTING' /> : <ButtonBar formName='expectrequestForm' />}

            </Form>

        )

    }

}

export default reduxForm({form: 'expectrequestForm', onSubmit: sendExpectRequest, destroyOnUnmount: false})(ExpectRequestForm);
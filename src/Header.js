import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-desktop/windows';
import './App.css';
import UTCClock from './UTCClock';

const Header = (props) => {

    const {label, text} = props;

    return (
        <View style={{width: '99%', height: '100%', flex: 1, flexDirection: 'row', marginLeft: 10}}>
            <UTCClock />
            <View style={{flex: 0.75, flexGrow: 1, color: 'white', alignSelf: 'center', justifyContent: 'center', fontSize: 30,}}>{label}</View>
            <View style={{flex: 0.25, color: 'white', justifyContent: 'flex-end', fontSize: 30,}}>{text}</View>
        </View>
    )

}

Header.propTypes = {
    label: PropTypes.string.isRequired,
    text: PropTypes.string,
}

export default Header;
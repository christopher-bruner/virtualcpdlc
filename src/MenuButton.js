import React from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router';
import {View} from 'react-desktop/windows';
import './App.css';

const MenuButton = (props) => {

    const {
        className,
        history,
        location,
        match,
        staticContext,
        to,
        onClick,

        ...rest
      } = props

      return (
        <View
            {...rest}
            onClick={(event) => {
                onClick && onClick(event)
                history.push(to)
            }} 
            className={to ? location.pathname.includes(to) ? 'Button-green' : 'Button-gray' : 'Button-cyan'} margin='3px' style={{textAlign: 'center', justifyContent: 'center', flexGrow: 1, width: '30%',}} />
      )
 
}
MenuButton.propTypes = {
    to: PropTypes.string,
    children: PropTypes.node.isRequired,
}

export default withRouter(MenuButton);
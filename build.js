var electronInstaller = require('electron-winstaller');

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: './dist/win-unpacked', //directory of app after running 'electron pack'
    outputDirectory: './dist/installer',
    noMsi: true,
    setupExe: 'virtualCPDL_Setup',
    exe: 'virtualCPDL',
    skipUpdateIcon: true,
//    iconUrl: '',
//    setupIcon: '',
//    loadingGif: '',
    
});

resultPromise.then(() => console.log('Success'), (e) => console.log(`error: ${e.message}`));